package org.gobii.digester.deglutator.loader;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.gobii.Util;
import org.postgresql.copy.CopyIn;
import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;

public class PostgresMetadataLoader implements Loader {

	private static final int BUFFER_SIZE = 256;

	private Connection conn;
	private CopyIn copyIn;
	private String table;
	private List<String> header;

	private Set<String> dups;
	private Map<String, org.gobii.digester.deglutator.loader.NMap> nmaps;

	private String holdingTableName;

	public PostgresMetadataLoader(String dbUrl, String table, List<String> header) throws Exception {

		conn = DriverManager.getConnection(dbUrl);
		this.table = table;
		this.header = header;


		dups = readDups(table);
		nmaps = readMap(table);
		holdingTableName = createTempTable(conn, table, nmaps);

		CopyManager copyManager = new CopyManager((BaseConnection) conn);
		copyIn = copyManager.copyIn(String.format("COPY %s FROM STDIN (FORMAT csv, DELIMITER E'\t', HEADER, QUOTE '\"', ESCAPE '\\')", holdingTableName));

	}

//	@Data
//	private static class NMap {
//
//		private List<String> fileColumns;
//		private String dbColumn;
//		private String referenceTable;
//		private List<String> referenceColumns;
//		private String referenceId;x
//
//	}

	public  Map<String, NMap> readMap(String table) throws IOException, URISyntaxException {

		String mapContent = Util.slurp(this.getClass().getClassLoader().getResourceAsStream(String.format("%s/%s.nmap", "ifl_map", table)));

		final Map<String, NMap> map = new HashMap<>();

		Arrays.stream(mapContent.split("\n"))
				.filter(line -> ! line.startsWith("#"))
				.forEach(line -> {
					String[] toks = line.split("\t");
					NMap nmap = new NMap();

					nmap.setReferenceEquality(Util.zipmap(parseColumn(toks[0]), parseColumn(toks[3])));
					nmap.setValueColumn(toks[1]);
					nmap.setReferenceTable(toks[2]);
					nmap.setReferenceColumn(toks[4]);

					nmap.getReferenceEquality().keySet()
							.forEach(fc -> map.put(fc, nmap));
				});

		return map;
	}

	public Set<String> readDups(String table) throws IOException {
		String mapContent = Util.slurp(this.getClass().getClassLoader().getResourceAsStream(String.format("%s/%s.dupmap", "ifl_map", table)));

		return Arrays.stream(mapContent.split("\n"))
				.filter(line -> ! line.startsWith("#"))
				.map(line -> line.split("\t")[0])
				.collect(Collectors.toSet());
	}

	public Map<String, Map<String, String>> columnDefs(Connection conn, String table) throws SQLException {

		Map<String, Map<String, String>> columnDefs = new HashMap<>();

		try(ResultSet rs = conn.getMetaData().getColumns(null, null, table, null)) {
			while (rs.next()) {
				String columnName = rs.getString("COLUMN_NAME");
				String dataType = rs.getString("TYPE_NAME");

				Map<String, String> columnDef = new HashMap<>();
				columnDef.put("COLUMN_NAME", columnName);
				columnDef.put("TYPE_NAME", dataType);
				columnDef.put("PRIMARY_KEY", rs.getString("COLUMN_NAME").equals(table + "_id") + "");
				columnDefs.put(columnName, columnDef);
			}
		}

		return columnDefs;
	}

	public String createTempTable(Connection conn, String table, Map<String, NMap> nmaps) throws SQLException {

		Map<String, Map<String, String>> columnDefs = columnDefs(conn, table);

		List<String> columnDefStatements = new LinkedList<>();

 		for (String column : header) {

			String columnName = column;
			String dataType = "TEXT";
			String options = "";

			if (columnDefs.containsKey(column)) {
				columnName = columnDefs.get(column).get("COLUMN_NAME");
				dataType = columnDefs.get(column).get("TYPE_NAME");
			}

			if (nmaps != null && nmaps.containsKey(columnName)) {
				NMap nmap = nmaps.get(columnName);

				dataType = columnDefs(conn, nmap.getReferenceTable())
												.get(nmap.getReferenceEquality().get(column))
												.get("TYPE_NAME");
			}

			columnDefStatements.add(String.format("%s %s %s", columnName, dataType, options));
		}

		String tempTableName = String.format("%s__%s", table, UUID.randomUUID().toString().replace('-', '_'));

		String command = String.format("CREATE TEMPORARY TABLE %s %s",
				tempTableName,
				String.format("(%s)", String.join(", ", columnDefStatements)));

		conn.prepareStatement(command).execute();

		return tempTableName;
	}

	private List<String> parseColumn(String column) {
		return Arrays.stream(column.split(",")).map(String::trim).collect(Collectors.toList());
	}

	private String onPredicate(String fromTable, NMap nmap, AtomicInteger index) {

		List<String> onPredicates = new ArrayList<>();
		for (Map.Entry<String, String> equality : nmap.getReferenceEquality().entrySet()) {
			onPredicates.add(
					String.format("%s%s.%s = %s.%s",
						nmap.getReferenceTable(),
						index.get(),
						equality.getValue(),
						fromTable,
						equality.getKey()));
		}

		index.getAndIncrement();

		return String.join(" AND ", onPredicates);
	}

	private <T, R> List<T> selectFirstOfGroup(Function<T, R> grouper, List<T> list) {
		List<T> ret = new LinkedList<>();
		Set<R> selectedGroups = new HashSet<>();
		for (T t : list) {
			R r = grouper.apply(t);
			if (! selectedGroups.contains(r) || r == null) {
				ret.add(t);
				selectedGroups.add(r);
			}
		}

		return ret;
	}

	public String copyFromStatement(String fromTable, String toTable, Map<String, Map<String, String>> toTableColumnDefs,
										   Set<String> dups, Map<String, NMap> nmaps) {

		String distinct = String.join(", ", dups);

		AtomicInteger i = new AtomicInteger();

		String columns = selectFirstOfGroup(nmaps::get, header)
				.stream()
				.map(col -> nmaps.containsKey(col)
						? String.format("%s%s.%s as %s",
										nmaps.get(col).getReferenceTable(),
										i.getAndIncrement(),
										nmaps.get(col).getReferenceColumn(),
										nmaps.get(col).getValueColumn())
						: String.format("%s.%s", fromTable, col))
				.collect(Collectors.joining(", "));

		i.set(0);

		String joins =
				nmaps.values()
						.stream()
						.filter(nmap -> header.containsAll(nmap.getReferenceEquality().keySet()))
						.map(nmap -> String.format("LEFT JOIN %s AS %s%s ON %s",
								nmap.getReferenceTable(),
								nmap.getReferenceTable(),
								i.get(),
								onPredicate(fromTable, nmap, i)))
						.collect(Collectors.joining(" "));


		String select =
				String.format("SELECT whole.* FROM (SELECT %s %s FROM %s %s) AS whole",
						! dups.isEmpty()
								? String.format("DISTINCT ON (%s)", distinct)
								: "",
						columns,
						fromTable,
						joins);

		String dupJoins =
				! dups.isEmpty()
						?
						String.format("LEFT JOIN %s ON %s WHERE %s",
								toTable,
								dups.stream()
										.map(dup -> String.format("%s.%s = %s.%s", "whole", dup, toTable, dup))
										.collect(Collectors.joining(" AND ")),
								dups.stream()
										.map(dup -> String.format("%s.%s IS NULL", toTable, dup))
										.collect(Collectors.joining(" AND ")))
						: "";

		List<String> insertColumns = selectFirstOfGroup(nmaps::get, header);

		String command = String.format("INSERT INTO %s (%s) (%s %s)",
				toTable,
				insertColumns.stream()
						.map(c -> nmaps.containsKey(c)
								? nmaps.get(c).getValueColumn()
								: c)
						.collect(Collectors.joining(",")),
				select,
				dupJoins);

		System.out.println(command);
		return command;
	}

	public void copyFrom(Connection conn, String fromTable, String toTable,
								Set<String> dups, Map<String, NMap> nmaps) throws SQLException {

		if (toTable.startsWith("dataset_")) {
			toTable = toTable.substring(toTable.indexOf("_") + 1);
		}

		conn.prepareStatement(copyFromStatement(fromTable, toTable, columnDefs(conn, toTable), dups, nmaps))
				.execute();

	}


	@Override
	public void load(String s) throws Exception {
		byte[] bytes = (s + "\n").getBytes();
		try {
			copyIn.writeToCopy(bytes, 0, bytes.length);
			copyIn.flushCopy();
		} catch (SQLException e) {
			throw new Exception(e);
		}
	}

	@Override
	public void commit() throws Exception {

		try {
			copyIn.endCopy();
			copyFrom(conn, holdingTableName, table, dups, nmaps);
		} finally {
			if (copyIn.isActive()) {
				copyIn.cancelCopy();
			}
			conn.prepareStatement(String.format("DROP TABLE %s", holdingTableName))
					.execute();
		}
	}


}
