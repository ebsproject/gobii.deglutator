package org.gobii.digester.deglutator.loader;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

public class Hdf5MatrixLoader implements Loader {

	private static String path;

	private File temp;
	private File destination;

	private PrintWriter writer;

	private int size;

	public Hdf5MatrixLoader(File destination, int size) throws IOException {
		this.destination = destination;
		this.temp = File.createTempFile("matrix", "tsv");
		this.writer = new PrintWriter(new FileOutputStream(temp));
		this.size = size;
	}

	@Override
	public void load(String s) {
		this.writer.write(s);
		this.writer.write('\n');
		this.writer.flush();
	}

	@Override
	public void commit() throws Exception {

		Runtime.getRuntime().exec(String.format("loadHDF5 %s %s %s", size, temp.getAbsolutePath(), destination.getAbsolutePath()));
	}
}
